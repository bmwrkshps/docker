# Docker Compose
Compose is a tool for defining and running multi-container Docker applications.

* Base command:
  * `docker compose`
* Compose file `docker-compose.yml`
* Run composition:
  * `docker compose up -d`
* Stop composition
  * `docker compose stop`
* Start stopped composition
  * `docker compose start`
* Remove composition
  * `docker compose down`