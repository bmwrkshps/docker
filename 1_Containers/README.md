# Docker Container

# CLI
* Container management command base:
  * docker container
  * docker run
  * docker exec
* List running containers:
  * docker container ls
* List ALL containers:
  * docker container ls -a
* Start new container:
  * docker run mysql
* Remove stopped container
  * docker container rm <container name>
* Start new container properly
  * RTFM ![dockerhub](https://hub.docker.com/_/mysql)
  * `
  * docker run --name mymysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=rootpass -e MYSQL_DATABASE=sample -e MYSQL_USER=docker -e MYSQL_PASSWORD=baseline -d mysql:8.0.27`
* Stop/Start container
  * docker container stop <name or id>
  * docker container start <name or id>
* Execute binary in container
  * `docker exec -it mymysql /bin/sh`
* Remove stopped container
  * docker container rm <name or id>
* Inspect container
  * docker inspect XXX
* Remove container (Force)
  * docker container rm XXX -f
* Running container STATS (this is out-of-scope, but short bonus)
  * docker stats

# CLI aliases

* docker ps -a
* docker stop
* docker rm
* docker inspect