# Docker Baseline
Here you can find materials and instructions for short docker course that covers Docker basics.
![Docker](https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png "Docker visualized")
Topics covered in the workshop are:

1. Containers (50%)
2. Images (30%)
3. Volumes (5%)
4. Network (5%)
5. Compose (10%)

## Clear volumes - execute these when you finish your workshop
`rm -rf ./3_Volumes/data/*`

`rm -rf ./5_Compose/data/mysql/*`

`rm -rf ./5_Compose/data/wp/*`

### or just
`rm -rf ./3_Volumes/data/* && rm -rf ./5_Compose/data/mysql/* && rm -rf ./5_Compose/data/wp/*`