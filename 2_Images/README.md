# Docker Images
Docker images is a template for Docker container. It is a set of instructions to build and execute container.

* List all *local* images
  * `docker image ls`
* Build your own image
  * docker build . -t <your username>/<name of image>
  * docker build ./node_service -t darkovac/node_service
* Dockerfile
* .dockerignore
* Remove docker image
  * `docker image rm darkovac/node_service`
* Run our image (rebuild it first):
  * `docker run --name my_node_service -P darkovac/node_service`
  * ---
  * `docker run --name my_node_service -P -d -e DB_HOST=172.17.0.3 darkovac/node_service`
  * `docker exec -it my_node_service /bin/sh`
* Image registry: ![dockerhub](https://hub.docker.com/)

## Aliases
* docker images
* docker rmi
