import mysql from 'mysql2';

const connection = mysql.createConnection({
  host     : process.env.DB_HOST || 'localhost',
  port     : process.env.DB_PORT || '3306',
  user     : process.env.DB_USER || 'docker',
  password : process.env.DB_PASSWORD || 'baseline',
  database : process.env.DB_DB || 'sample'
});

export default connection;