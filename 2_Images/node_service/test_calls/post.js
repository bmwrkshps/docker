import http from 'http';

const options = {
    host: '127.0.0.1',
    port: process.argv[2] || 3000,
    path: '/',
    method: 'POST'
};

try {
    http.request(options, (res) => {
        console.log('STATUS: ' + res.statusCode);
        // console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('BODY: ' + chunk);
        });
    }).end();
} catch (error) {
    console.error(error);
}
