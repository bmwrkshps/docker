import log from './logger.js';

const counterDA = {
    createTable: (conn) => {
        return new Promise((resolve, reject) => {
            conn.query(`CREATE TABLE IF NOT EXISTS counter (
                id INT NOT NULL AUTO_INCREMENT,
                value INT NOT NULL DEFAULT 0,
                PRIMARY KEY (id));`,
                (error, results, fields) => {
                    if (error) reject(error);
                    else {
                        log.info(`Table created if it didn't exist`);
                        resolve(results);
                    }
                });
        });
    },
    newCounter: (conn) => {
        return new Promise((resolve, reject) => {
            conn.query(`INSERT INTO counter SET value = 0`,
                (error, results, fields) => {
                    if (error) reject(error);
                    else resolve(results.insertId);
                });
        });
    },
    getCounterValue: (conn, id) => {
        return new Promise((resolve, reject) => {
            conn.query(`SELECT value from counter WHERE id = ${id}`,
                (error, results, fields) => {
                    if (error) reject( error);
                    else resolve(results[0].value);
                });
        });
    },
    incrementCounter: (conn, id) => {
        return new Promise((resolve, reject) => {
            conn.query(`UPDATE counter SET value = value + 1 WHERE id = ${id}`,
            (error, results, fields) => {
                if (error) reject( error);
                else resolve();
            });
        });
    }
}

export default counterDA;