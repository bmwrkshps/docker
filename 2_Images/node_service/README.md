# DISCLAIMER
*This is simple sample service to show some Docker functionalities. Do not use it as example of good coding practices or as base for some future node project!*

# REQUIREMENTS
MySQL database. 
Connection should be set up in `./database-connection.js`

# INITIALIZE
Run:

`npm install`

# START
Run:

`npm start`

# INVOKE
Each script can be executed multiple times:
```
node test_calls/post.js
node test_calls/put.js
node test_calls/get.js
```