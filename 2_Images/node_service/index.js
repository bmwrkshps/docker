import http from 'http';

import log from './logger.js';
import conn from './database-connection.js';
import counterDA from './counter-data-access.js';
import logger from './logger.js';

function getIdFromURL(url) {
    if(url && url !== '/'){
        const id = parseInt(url.substring(1), 10);
        return id;
    }
    return null;
}

try {
    logger.info('STARTING...')
    counterDA.createTable(conn).then(() => {
        const hostname = '127.0.0.1';
        const port = 3000;

        const server = http.createServer((req, res) => {
            logger.info([req.method, req.url]);
            const id = getIdFromURL(req.url);
            if (req.method === 'GET' && id) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/plain');
                counterDA.getCounterValue(conn, id)
                    .then(id => {
                        res.end(`${id}`);
                    });
            } else if (req.method === 'POST' && req.url === '/') {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/plain');
                counterDA.newCounter(conn)
                    .then(id => {
                        res.end(`${id}`);
                    });
            } else if (req.method === 'PUT' && id) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/plain');
                counterDA.incrementCounter(conn, id)
                    .then(() => {
                        res.end();
                    });
            } else {
                res.statusCode = 404;
                res.setHeader('Content-Type', 'text/plain');
                res.end('Endpoint does not exist!');
            }
        });

        server.listen(port, hostname, () => {
            logger.info(`Server running at http://${hostname}:${port}/`);
        });
    }).catch(err => log.error(err));
} catch (err) {
    logger.error(err);
}


